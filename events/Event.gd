extends Node
class_name Event

var id : String = ""
var conversation_original : String = ""
var conversation : String = ""
var personal : bool = false
var reusable : bool = false
var options : Array = []


func add_option(text : String, action : String) -> EventOption:
	var option = EventOption.new()
	option.text_original = text
	option.text = text
	option.action = action
	options.append(option)
	return option


func apply_variables(character : Character) -> void:
	conversation = conversation_original
	return


func is_ready(character : Character) -> bool:
	return true


func apply_option(action : String) -> void:
	pass
