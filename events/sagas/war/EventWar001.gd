extends "../../Event.gd"


func _init() -> void:
	id = "EV_WAR_START"
	conversation_original = "The next door kingdom seems to want ot invade us. What should we do?"
	
	var last_option : EventOption = null
	
	last_option = add_option("Send a spy and check the true", "EV_WAR_SPY")
	last_option.gold_change = -20
	
	last_option = add_option("Prepare our troops", "EV_WAR_BATTLE")
	last_option.army_change = -5
	last_option.ethic = 1
	
	last_option = add_option("Ignore the problem", "EV_WAR_IGNORE")
	last_option.popularity = -10
	last_option.ethic = -1
