extends "../../Event.gd"


func _init() -> void:
	id = "EV_WAR_IGNORE"
	conversation_original = "The enemy's troops are here and we didn't prepare anything. This is not looking good... My king, what we can do?"
	
	var last_option : EventOption = null
	
	last_option = add_option("Battle at all cost", "")
	last_option.popularity = 20
	last_option.army_change = -10
	last_option.ethic = 1
	
	last_option = add_option("Don't care. This is the life", "")
	last_option.popularity = -30
	last_option.ethic = -1
	last_option.moral = -1


func is_ready(character : Character) -> bool:
	if History.actions.has("EV_WAR_IGNORE"):
		return true
	return false
