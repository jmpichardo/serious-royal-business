extends "../../Event.gd"


func _init() -> void:
	id = "EV_WAR_PRAY_CONTINUE"
	conversation_original = "A demigod has appeared in the enemy's castle killing everyone, soldiers and not soldiers. Our rivals are totally gone!"
	
	var last_option : EventOption = null
	
	last_option = add_option("Enjoy and make a party", "")
	last_option.gold_change = -20
	last_option.popularity = 30
	last_option.ethic = -1
	last_option.moral = -1
	
	last_option = add_option("Blame for not believe in you", "")
	last_option.nice = -10
	last_option.ethic = -1
	
	last_option = add_option("Sit down and pray a bit more", "")
	last_option.ethic = -1
	last_option.moral = 1


func is_ready(character : Character) -> bool:
	if History.actions.has("EV_WAR_PRAY_CONTINUE"):
		return true
	return false
