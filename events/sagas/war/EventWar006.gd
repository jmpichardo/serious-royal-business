extends "../../Event.gd"


func _init() -> void:
	id = "EV_WAR_PRAY"
	conversation_original = "The people and army are a bit affraid about your praying acts. They think we should do something more."
	
	var last_option : EventOption = null
	
	last_option = add_option("Ignore them, keep praying", "EV_WAR_PRAY_CONTINUE")
	last_option.popularity = -10
	last_option.ethic = -1
	
	last_option = add_option("Ok, time to send some men to the battle", "EV_WAR_BATTLE")
	last_option.army_change = -5
	last_option.popularity = 10
	last_option.ethic = 1


func is_ready(character : Character) -> bool:
	if History.actions.has("EV_WAR_PRAY"):
		return true
	return false
