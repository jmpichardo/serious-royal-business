extends "../../Event.gd"


func _init() -> void:
	id = "EV_WAR_BATTLE"
	conversation_original = "The fight is fierce. Our men are doing great, but the battle is no over, we need more instructions from your majesty."
	
	var last_option : EventOption = null
	
	last_option = add_option("Let them fight until the end", "")
	last_option.ethic = -1
	last_option.moral = -1
	
	last_option = add_option("Send some more men to the front and win the war", "")
	last_option.army_change = -1
	last_option.popularity = 30
	last_option.ethic = 1
	last_option.moral = 1

func is_ready(character : Character) -> bool:
	if History.actions.has("EV_WAR_BATTLE"):
		return true
	return false
