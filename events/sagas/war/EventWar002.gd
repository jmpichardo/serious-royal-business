extends "../../Event.gd"


func _init() -> void:
	id = "EV_WAR_SPY"
	conversation_original = "Our spy found that they are preparing a war against us. They have troops, catapults, pigs for food..."
	
	var last_option : EventOption = null
	
	last_option = add_option("Prepare our men", "EV_WAR_BATTLE")
	last_option.army_change = -5
	last_option.ethic = 1
	
	last_option = add_option("Poison the pigs", "EV_WAR_POISON")
	last_option.popularity = 20
	last_option.ethic = -1
	
	last_option = add_option("Pray to go", "EV_WAR_PRAY")
	last_option.popularity = -20
	last_option.ethic = -1
	last_option.moral = 1


func is_ready(character : Character) -> bool:
	if History.actions.has("EV_WAR_SPY"):
		return true
	return false
