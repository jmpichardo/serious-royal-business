extends "../../Event.gd"


func _init() -> void:
	id = "EV_WAR_POISON"
	conversation_original = "The pig poisoning is working! Our enemy forces languish."
	
	var last_option : EventOption = null
	
	last_option = add_option("Poison cows as well and make them starve", "")
	last_option.ethic = -1
	last_option.moral = -1
	
	last_option = add_option("Time to send some soldiers and finish this", "")
	last_option.army_change = -1
	last_option.popularity = 10


func is_ready(character : Character) -> bool:
	if History.actions.has("EV_WAR_POISON"):
		return true
	return false
