extends "../../Event.gd"


func _init() -> void:
	id = "EV_PIGS_RIBEYE"
	conversation_original = "Your greatness, I was thinking that it could be a good idea if we give ribeye steaks to the pigs."
	personal = true
	
	var last_option : EventOption = null
	
	last_option = add_option("Sounds good", "")
	last_option.gold_change = -10
	last_option.ethic = -1
	
	last_option = add_option("No, thank you...", "")
	last_option.ethic = 1
	
	last_option = add_option("This sounds stupid!", "")
	last_option.nice = -10
	last_option.ethic = 1
	last_option.moral = -1
