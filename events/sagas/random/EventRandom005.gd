extends "../../Event.gd"


func _init() -> void:
	id = "EV_FESTIVAL"
	conversation_original = "Your majesty, people wants to celebrate a festival for their gods. Would you help them?"
	reusable = true
	
	var last_option : EventOption = null
	
	last_option = add_option("Give some gold", "")
	last_option.gold_change = -10
	last_option.popularity = 10
	last_option.moral = 1
	
	last_option = add_option("Give some goods you don't use", "")
	last_option.popularity = 5
	last_option.moral = -1
	
	last_option = add_option("Decline any help", "")
	last_option.popularity = -5
