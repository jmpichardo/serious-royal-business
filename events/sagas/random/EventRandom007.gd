extends "../../Event.gd"


func _init() -> void:
	id = "EV_HIRE_SOLDIERS"
	conversation_original = "Your majesty, Would you like to get some extra men?"
	reusable = true
	
	var last_option : EventOption = null
	
	last_option = add_option("Hire a lot more soldiers", "")
	last_option.gold_change = -60
	last_option.army_change = 5
	
	last_option = add_option("Hire more soldiers", "")
	last_option.gold_change = -30
	last_option.army_change = 2
	
	last_option = add_option("No, thank you", "")
