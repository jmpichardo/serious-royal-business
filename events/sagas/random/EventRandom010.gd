extends "../../Event.gd"


func _init() -> void:
	id = "EV_DARKNESS_SCARE"
	conversation_original = "Darkness scares me, Could I borrow some money to put some torches in my room?"
	personal = true
	
	var last_option : EventOption = null
	
	last_option = add_option("Yes", "")
	last_option.gold_change = -10
	last_option.moral = 1
	
	last_option = add_option("No", "")
	last_option.nice = -10
	
