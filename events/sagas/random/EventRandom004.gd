extends "../../Event.gd"


func _init() -> void:
	id = "EV_PEOPLE_DEMANDS"
	conversation_original = "Your majesty, people is not very happy. They demand you to give something to them."
	reusable = true
	
	var last_option : EventOption = null
	
	last_option = add_option("Give some gold", "")
	last_option.gold_change = -10
	last_option.popularity = 10
	last_option.ethic = -1
	last_option.moral = 1
	
	last_option = add_option("Give a lot of gold", "")
	last_option.gold_change = -40
	last_option.popularity = 20
	last_option.ethic = 1
	last_option.moral = 1
	
	last_option = add_option("Kill someone to teach them a lesson", "")
	last_option.popularity = -20
	last_option.ethic = -1
	last_option.moral = -1
	
	last_option = add_option("Use army to put some order", "")
	last_option.army_change = -1
	last_option.ethic = 1
	last_option.moral = -1
