extends "../../Event.gd"


func _init() -> void:
	id = "EV_PAYRISE"
	conversation_original = "Your majesty, it's been a hard week. Could I borrow some gold?"
	personal = true
	reusable = true
	
	var last_option : EventOption = null
	
	last_option = add_option("Give gold", "")
	last_option.nice = 5
	last_option.gold_change = -10
	
	last_option = add_option("Don't give gold", "")
	last_option.nice = -5
