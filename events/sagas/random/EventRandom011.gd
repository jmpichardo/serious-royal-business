extends "../../Event.gd"


func _init() -> void:
	id = "EV_QUIZ_2"
	conversation_original = "What happens in the gray zone between solid and liquid?"
	reusable = true
	
	var last_option : EventOption = null
	
	last_option = add_option("It gets hot there", "")
	last_option.ethic = -1
	
	last_option = add_option("Probably nothing", "")
	last_option.nice = -5
	last_option.ethic = -1
	
	last_option = add_option("We have no answers for that question", "")
	last_option.nice = 5
	last_option.ethic = 1
