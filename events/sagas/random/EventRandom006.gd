extends "../../Event.gd"


func _init() -> void:
	id = "EV_QUEST"
	conversation_original = "Our hero {hero_name} wants to start a new expedition and is asking for some financing."
	reusable = true
	
	var last_option : EventOption = null
	
	last_option = add_option("Give some gold", "")
	last_option.gold_change = -20
	last_option.popularity = 5
	last_option.ethic = 1
	last_option.moral = 1
	
	last_option = add_option("Give some soldiers", "")
	last_option.army_change = -1
	last_option.popularity = 5
	last_option.ethic = 1
	
	last_option = add_option("Decline any help", "")
	last_option.moral = -1
	
	last_option = add_option("Kill the hero. No more bleeding money.", "EV_QUEST_KILL_HERO")
	last_option.ethic = -1
	last_option.moral = -1
	last_option.popularity = -20


func apply_variables(character : Character) -> void:
	conversation = conversation_original.replace("{hero_name}", History.hero.creature_name)


func is_ready(character : Character) -> bool:
	if !History.hero.is_dead:
		return true
	return false


func apply_option(action : String) -> void:
	if action == "EV_QUEST_KILL_HERO":
		History.hero.is_dead = true
