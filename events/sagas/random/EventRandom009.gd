extends "../../Event.gd"


func _init() -> void:
	id = "EV_QUIZ_1"
	conversation_original = "What do you think is faster? A goose or a cynopithecus?"
	personal = true
	
	var last_option : EventOption = null
	
	last_option = add_option("The goose", "")
	last_option.ethic = -1
	
	last_option = add_option("The cynopithecus", "")
	last_option.ethic = 1
	
	last_option = add_option("This question is useless", "")
	last_option.nice = -10
