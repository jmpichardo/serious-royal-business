extends "../../Event.gd"


func _init() -> void:
	id = "EV_WELCOME"
	conversation_original = "Greetings! My name is {char_name}. I'm here to serve you."
	personal = true
	
	var last_option : EventOption = null
	
	last_option = add_option("Warm welcome", "")
	last_option.moral = 2
	
	last_option = add_option("Be rude", "EV_WELCOME_RUDE")
	last_option.moral = -2


func apply_variables(character : Character) -> void:
	if character != null:
		conversation = conversation_original.replace("{char_name}", character.char_name)
