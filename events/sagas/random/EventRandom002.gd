extends "../../Event.gd"


func _init() -> void:
	id = "EV_WELCOME_APOLOGIZE"
	conversation_original = "I didn't like how you answer me the other day."
	personal = true
	
	var last_option : EventOption = null
	
	last_option = add_option("Say sorry", "")
	last_option.moral = 2
	
	last_option = add_option("Ingore it", "")
	last_option.moral = -2


func is_ready(character : Character) -> bool:
	if character.action_history.has("EV_WELCOME_RUDE"):
		return true
	return false
