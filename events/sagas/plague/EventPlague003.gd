extends "../../Event.gd"


func _init() -> void:
	id = "EV_PLAGUE_INVESTIGATE"
	conversation_original = "After some investigations we found the origin of the plague. A witch in the forest is poisoning the river."
	
	var last_option : EventOption = null
	
	last_option = add_option("Send some soldiers to kill the witch", "")
	last_option.army_change = -2
	last_option.ethic = 1
	
	last_option = add_option("Let's talk with the witch", "EV_PLAGUE_INVESTIGATE_TALK")
	last_option.popularity = -30
	last_option.ethic = -1
	last_option.moral = -1


func is_ready(character : Character) -> bool:
	if History.actions.has("EV_PLAGUE_INVESTIGATE"):
		return true
	return false
