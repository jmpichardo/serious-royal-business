extends "../../Event.gd"


func _init() -> void:
	id = "EV_PLAGUE_START"
	conversation_original = "The amount of people getting sick is increasing and we don't know the reason."
	
	var last_option : EventOption = null
	
	last_option = add_option("Send some soldiers to investigate", "EV_PLAGUE_INVESTIGATE")
	last_option.army_change = -1
	last_option.ethic = 1
	
	last_option = add_option("Send some doctors to cure people", "EV_PLAGUE_INVESTIGATE")
	last_option.gold_change = -30
	last_option.ethic = 1
	last_option.moral = 1
	
	last_option = add_option("Ignore the problem", "EV_PLAGUE_IGNORE")
	last_option.popularity = -20
	last_option.moral = -1

