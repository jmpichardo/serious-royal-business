extends "../../Event.gd"


func _init() -> void:
	id = "EV_PLAGUE_INVESTIGATE_BASILIKS"
	conversation_original = "We lost all our men trying to find the basilik's head... at least the poisoning has stopped for some reason."
	
	var last_option : EventOption = null
	
	last_option = add_option("Commemorate fallen ones", "")
	last_option.gold_change = -10
	last_option.popularity = 10
	last_option.ethic = 1
	last_option.moral = 1
	
	last_option = add_option("Don't tell anyone about our failure", "")
	last_option.ethic = -1
	last_option.moral = -1


func is_ready(character : Character) -> bool:
	if History.actions.has("EV_PLAGUE_INVESTIGATE_BASILIKS"):
		return true
	return false
