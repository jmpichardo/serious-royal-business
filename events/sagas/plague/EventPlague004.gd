extends "../../Event.gd"


func _init() -> void:
	id = "EV_PLAGUE_INVESTIGATE_TALK"
	conversation_original = "The witch will stop the poisoning if we give her a basilik's head."
	
	var last_option : EventOption = null
	
	last_option = add_option("Send some soldiers to find the head", "EV_PLAGUE_INVESTIGATE_BASILIKS")
	last_option.army_change = -2
	last_option.popularity = 20
	last_option.ethic = 1
	
	last_option = add_option("Give her gold instead", "")
	last_option.gold_change = -30
	last_option.popularity = -10


func is_ready(character : Character) -> bool:
	if History.actions.has("EV_PLAGUE_INVESTIGATE_TALK"):
		return true
	return false
