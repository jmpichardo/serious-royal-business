extends "../../Event.gd"


func _init() -> void:
	id = "EV_PLAGUE_IGNORE"
	conversation_original = "Many people is still dying because of the plague. But now, some of them are starting to act strange and bitting others."
	
	var last_option : EventOption = null
	
	last_option = add_option("Send a hero to find the cure", "EV_PLAGUE_INVESTIGATE")
	last_option.gold_change = -20
	last_option.moral = 1
	
	last_option = add_option("Kill anyone with symptoms", "")
	last_option.popularity = -30
	last_option.ethic = -1
	last_option.moral = -1


func is_ready(character : Character) -> bool:
	if History.actions.has("EV_PLAGUE_IGNORE"):
		return true
	return false
