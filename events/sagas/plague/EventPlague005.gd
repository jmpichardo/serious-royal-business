extends "../../Event.gd"


func _init() -> void:
	id = "EV_PLAGUE_INVESTIGATE_BASILIKS"
	conversation_original = "Our men found the basilik's head and gave it to the witch. As she promised, the water has no poison anymore."
	
	var last_option : EventOption = null
	
	last_option = add_option("Send some gold to the soldiers", "")
	last_option.gold_change = -20
	last_option.popularity = 10
	last_option.ethic = 1
	last_option.moral = 1
	
	last_option = add_option("Let's celebrate with a crazy party!", "")
	last_option.gold_change = -30
	last_option.popularity = 15
	last_option.ethic = -1
	
	last_option = add_option("Do nothing", "")
	last_option.ethic = 1


func is_ready(character : Character) -> bool:
	if History.actions.has("EV_PLAGUE_INVESTIGATE_BASILIKS"):
		return true
	return false
