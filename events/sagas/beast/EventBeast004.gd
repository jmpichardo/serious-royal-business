extends "../../Event.gd"


func _init() -> void:
	id = "EV_BEAST_TRIBUTE_PROBLEM"
	conversation_original = "When you send commoners as tribute to {beast_name}, one of them survived and now he is erected as a hero and looking for revenge making people angry against you."
	
	var last_option : EventOption = null
	
	last_option = add_option("Give him some money to stop", "")
	last_option.gold_change = -40
	last_option.ethic = -1
	last_option.moral = 1
	
	last_option = add_option("Jail him", "")
	last_option.army_change = -1
	last_option.popularity = -20
	last_option.ethic = 1
	
	last_option = add_option("Kill him", "EV_BEAST_TRIBUTE_PROBLEM_KILL")
	last_option.popularity = -30
	last_option.ethic = -1
	last_option.moral = -1


func apply_variables(character : Character) -> void:
	conversation = conversation_original.replace("{beast_name}", History.beast.creature_name)


func is_ready(character : Character) -> bool:
	if History.actions.has("EV_BEAST_WELCOME_HUMANS"):
		return true
	return false
