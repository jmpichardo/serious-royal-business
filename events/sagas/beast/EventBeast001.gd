extends "../../Event.gd"


func _init() -> void:
	id = "EV_BEAST_START"
	conversation_original = "Your majesty. A {beast_name} has been detected approaching the kingdom. What should we do?"
	
	var last_option : EventOption = null
	
	last_option = add_option("Prepare the army", "EV_BEAST_WELCOME_ARMY")
	last_option.army_change = -3
	last_option.ethic = 1
	last_option.moral = -1
	
	last_option = add_option("Give some gold", "")
	last_option.gold_change = -50
	last_option.ethic = 1
	last_option.moral = 0
	
	last_option = add_option("Give some goods as tribute", "")
	last_option.popularity = -20
	last_option.ethic = -1
	last_option.moral = 1
	
	last_option = add_option("Give some commoners as tribute", "EV_BEAST_WELCOME_HUMANS")
	last_option.popularity = -40
	last_option.ethic = -1
	last_option.moral = -1


func apply_variables(character : Character) -> void:
	var beast : Creature = History.beast
	conversation = conversation_original.replace("{beast_name}", beast.creature_name)
