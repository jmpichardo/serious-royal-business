extends "../../Event.gd"


func _init() -> void:
	id = "EV_BEAST_WELCOME_ARMY"
	conversation_original = "Your majesty. The {beast_name} has destroyed our army and still coming to us!"
	
	var last_option : EventOption = null
	
	last_option = add_option("Send more men", "EV_BEAST_WELCOME_ARMY_MORE_ARMY")
	last_option.army_change = -3
	last_option.popularity = 10
	last_option.ethic = 1
	last_option.moral = -1
	
	last_option = add_option("Give some gold instead", "")
	last_option.gold_change = -200
	last_option.ethic = 1
	last_option.moral = 0
	
	last_option = add_option("Give some goods as tribute", "")
	last_option.popularity = -20
	last_option.ethic = -1
	last_option.moral = 1
	
	last_option = add_option("Give some commoners as tribute", "EV_BEAST_WELCOME_HUMANS")
	last_option.popularity = -40
	last_option.ethic = -1
	last_option.moral = -1


func apply_variables(character : Character) -> void:
	conversation = conversation_original.replace("{beast_name}", History.beast.creature_name)


func is_ready(character : Character) -> bool:
	if History.actions.has("EV_BEAST_WELCOME_ARMY"):
		return true
	return false
