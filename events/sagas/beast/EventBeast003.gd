extends "../../Event.gd"


func _init() -> void:
	id = "EV_BEAST_WELCOME_ARMY"
	conversation_original = "Your majesty. The {beast_name} has been defeated! Should we celebrate?"
	
	var last_option : EventOption = null
	
	last_option = add_option("Prepare a party for everyone", "")
	last_option.gold_change = -40
	last_option.popularity = 20
	last_option.moral = 1
	
	last_option = add_option("Prepare a party for us only", "")
	last_option.gold_change = -20
	last_option.popularity = -5
	last_option.ethic = 1
	last_option.moral = 0
	
	last_option = add_option("Do nothing", "")
	last_option.popularity = -10


func apply_variables(character : Character) -> void:
	conversation = conversation_original.replace("{beast_name}", History.beast.creature_name)


func is_ready(character : Character) -> bool:
	if History.actions.has("EV_BEAST_WELCOME_ARMY") or History.actions.has("EV_BEAST_WELCOME_ARMY_MORE_ARMY"):
		return true
	return false
