extends "../../Event.gd"


func _init() -> void:
	id = "EV_BEAST_TRIBUTE_PROBLEM_KILL"
	conversation_original = "We couldn't kill the self-erected hero {hero_name} and now he is stronger. People pray to him as a real hero."
	
	var last_option : EventOption = null
	
	last_option = add_option("Accept it", "")
	last_option.popularity = -10
	last_option.ethic = 1
	last_option.moral = 1
	
	last_option = add_option("Blame", "")
	last_option.nice = -10
	last_option.moral = -1


func apply_variables(character : Character) -> void:
	History.hero.generate(false)
	conversation = conversation_original.replace("{hero_name}", History.hero.creature_name)


func is_ready(character : Character) -> bool:
	if History.actions.has("EV_BEAST_TRIBUTE_PROBLEM_KILL"):
		return true
	return false
