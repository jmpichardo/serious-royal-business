extends Node

var used_events : Array = []
var actions : Array = []

var beast : Creature = null
var hero : Creature = null


func _ready() -> void:
	generate_world()


func generate_world() -> void:
	used_events = []
	actions = []
	beast = generate_creature(true)
	hero = generate_creature(false)


func generate_creature(is_beast : bool) -> Creature:
	var creature = Creature.new()
	creature.generate(is_beast)
	return creature
