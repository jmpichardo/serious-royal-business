extends Node
class_name Creature

var creature_name : String = ""
var is_dead : bool = false


func generate(is_beast : bool) -> void:
	is_dead = false
	if is_beast:
		creature_name = NameGenerator.generate_beast_name()
	else:
		creature_name = NameGenerator.generate_name()
