extends Node

var events : Array = []


func _ready() -> void:
	events.append(load("res://events/sagas/random/EventRandom001.gd").new())
	events.append(load("res://events/sagas/random/EventRandom002.gd").new())
	events.append(load("res://events/sagas/random/EventRandom003.gd").new())
	events.append(load("res://events/sagas/random/EventRandom004.gd").new())
	events.append(load("res://events/sagas/random/EventRandom005.gd").new())
	events.append(load("res://events/sagas/random/EventRandom006.gd").new())
	events.append(load("res://events/sagas/random/EventRandom007.gd").new())
	events.append(load("res://events/sagas/random/EventRandom008.gd").new())
	events.append(load("res://events/sagas/random/EventRandom009.gd").new())
	events.append(load("res://events/sagas/random/EventRandom010.gd").new())
	events.append(load("res://events/sagas/random/EventRandom011.gd").new())
	
	events.append(load("res://events/sagas/beast/EventBeast001.gd").new())
	events.append(load("res://events/sagas/beast/EventBeast002.gd").new())
	events.append(load("res://events/sagas/beast/EventBeast003.gd").new())
	events.append(load("res://events/sagas/beast/EventBeast004.gd").new())
	events.append(load("res://events/sagas/beast/EventBeast005.gd").new())
	
	events.append(load("res://events/sagas/plague/EventPlague001.gd").new())
	events.append(load("res://events/sagas/plague/EventPlague002.gd").new())
	events.append(load("res://events/sagas/plague/EventPlague003.gd").new())
	events.append(load("res://events/sagas/plague/EventPlague004.gd").new())
	events.append(load("res://events/sagas/plague/EventPlague005.gd").new())
	events.append(load("res://events/sagas/plague/EventPlague006.gd").new())
	
	events.append(load("res://events/sagas/war/EventWar001.gd").new())
	events.append(load("res://events/sagas/war/EventWar002.gd").new())
	events.append(load("res://events/sagas/war/EventWar003.gd").new())
	events.append(load("res://events/sagas/war/EventWar004.gd").new())
	events.append(load("res://events/sagas/war/EventWar005.gd").new())
	events.append(load("res://events/sagas/war/EventWar006.gd").new())
	events.append(load("res://events/sagas/war/EventWar007.gd").new())


func get_random_event(character : Character) -> Event:
	var available_events : Array = []
	
	if character.used_events.size() == 0 and character.action_history.size() == 0:
		return events[0]
	
	for event in events:
		if character.used_events.has(event.id):
			continue
		
		if History.used_events.has(event.id):
			continue
		
		if event.is_ready(character):
			available_events.append(event)
			continue
	
	if available_events.size() == 0:
		return null
	
	return available_events[randi() % available_events.size()]
