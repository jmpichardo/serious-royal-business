extends Node
class_name EventOption

var text_original : String = ""
var text : String = ""
var action : String = ""
var nice : int = 0
var gold_change : int = 0
var army_change : int = 0
var popularity : int = 0
var ethic : int = 0
var moral : int = 0
