extends Node2D

var GameScene : PackedScene = preload("res://Game.tscn")
var IntroScene : PackedScene = preload("res://Intro.tscn")

onready var MainMenuNode = $CanvasLayer/MainMenu
onready var StartMenuNode = $StartMenu
onready var MusicNode : AudioStreamPlayer = $Music

var current_scene : Node = null


func _physics_process(delta : float) -> void:
	randi()


func _on_ButtonStart_pressed():
	StartMenuNode.set_visible(false)
	MainMenuNode.set_visible(false)
	MusicNode.stop()
	History.generate_world()
	
	current_scene = IntroScene.instance()
	current_scene.connect("intro_finished", self, "_on_intro_finished")
	add_child(current_scene)


func _on_intro_finished() -> void:
	current_scene.queue_free()
	current_scene = GameScene.instance()
	current_scene.connect("game_ended", self, "_on_game_ended")
	add_child(current_scene)


func _on_game_ended() -> void:
	current_scene.queue_free()
	MainMenuNode.set_visible(true)
	StartMenuNode.set_visible(true)
	MusicNode.play()


func _on_ButtonExit_pressed():
	get_tree().quit()
