extends Node

signal game_ended

var CharacterScene = preload("res://people/Character.tscn")

onready var MainUINode = $CanvasLayer/MainUI
onready var CharactersNode = $Characters
onready var KilledByNode = $CanvasLayer/GameOverRect/KilledBy

var day : int = 1
var gold : int = 100
var army : int = 10
var popularity : int = 0

var characters : Array = []

var selected_character : Character = null
var current_event : Event = null


func _ready() -> void:
	MainUINode.connect("option_selected", self, "_on_option_selected")
	MainUINode.connect("event_finished", self, "_on_event_finished")
	MainUINode.connect("day_started", self, "_on_day_started")
	MainUINode.connect("character_killed", self, "_on_character_killed")
	
	_update_kingdom_values()
	_add_character()


func _add_character() -> void:
	var character : Character = CharacterScene.instance()
	MainUINode.connect("text_completed", character, "_on_text_completed")
	MainUINode.connect("text_started", character, "_on_text_started")
	character.connect("character_dead", self, "_on_character_dead")
	
	characters.append(character)
	CharactersNode.add_child(character)
	
	var total_chars = CharactersNode.get_child_count()
	var pos_x_distance : float = 8.0 / (total_chars + 1)
	var pos_x = -4.0
	
	for chara in CharactersNode.get_children():
		pos_x += pos_x_distance
		chara.set_original_position(pos_x)


func _select_character(character : Character) -> void:
	selected_character = character
	selected_character.select()


func _deselect_all() -> void:
	for chara in CharactersNode.get_children():
		chara.deselect()
		

func _update_kingdom_values() -> void:
	MainUINode.update_gold(gold)
	MainUINode.update_army(army)
	MainUINode.update_popularity(popularity)


func _on_day_started() -> void:
	gold += 5
	
	if randi() % 4 < 1 and CharactersNode.get_child_count() < 6:
		_add_character()
	
	if CharactersNode.get_child_count() == 0:
		_add_character()
	
	_update_kingdom_values()
	_deselect_all()
	for chara in CharactersNode.get_children():
		if chara.betrayal >= 100:
			chara.select()
			_you_die(chara.char_name)
			return
	
	if popularity <= -100:
		_you_die("your people")
		return
	
	var character = CharactersNode.get_child(randi() % CharactersNode.get_child_count())
	_select_character(character)
	
	current_event = EventStore.get_random_event(selected_character)
	if current_event != null:
		current_event.apply_variables(selected_character)
		MainUINode.load_event(current_event, gold, army)
	else:
		_on_event_finished()
	
	_update_debug()


func _on_option_selected(index : int) -> void:
	var answer : String = "..."
	var selected_option : EventOption = current_event.options[index]
	
	answer = selected_character.apply_option(current_event, selected_option)
	current_event.apply_option(selected_option.action)
	
	gold += selected_option.gold_change
	if gold < 0:
		gold = 0
	
	army += selected_option.army_change
	if army < 0:
		army = 0
	
	popularity += selected_option.popularity
	popularity = clamp(popularity, -100, 100)
	
	if !current_event.personal:
		if !current_event.reusable:
			History.used_events.append(current_event.id)
		if selected_option.action != "":
			History.actions.append(selected_option.action)
	
	MainUINode.load_answer(answer)
	_update_debug()
	_update_kingdom_values()


func _on_event_finished() -> void:
	_deselect_all()
	
	for chara in CharactersNode.get_children():
		chara.end_of_day()
	
	day += 1
	MainUINode.finish_day(day)


func _on_character_killed() -> void:
	if selected_character != null:
		MainUINode.clear_options()
		selected_character.kill()
		popularity -= 20
		popularity = clamp(popularity, -100, 100)
		_update_kingdom_values()


func _on_character_dead() -> void:
	if selected_character != null:
		selected_character.queue_free()
		_on_event_finished()


func _you_die(killer_name : String) -> void:
	KilledByNode.set_text("Assassinated by " + killer_name + "\nDays in the crown: " + str(day))
	$AnimationPlayer.play("die")


func _update_debug() -> void:
	$CanvasLayer/Debug/Label.set_text("Name: " + str(selected_character.char_name) 
						+ "\nAffinity:" + str(selected_character.affinity)
						+ "\nBetrayal:" + str(selected_character.betrayal)
						+ "\nAlignment:" + str(selected_character.get_alignment()))
	
#	print("Events")
#	print(History.used_events)
#	print("Actions")
#	print(History.actions)
#	print("-----------------------------------")


func _on_ButtonEnd_pressed():
	emit_signal("game_ended")
