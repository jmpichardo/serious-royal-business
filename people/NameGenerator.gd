extends Node

var human_chunks : Array = []

var beast_attributes : Array = []
var beast_names : Array = []


func _ready() -> void:
	human_chunks = ["tu", "ryx", "fan", "de", "ches", "ter", "ar", "ka", "yz", "se", "eds", "ta", "ke", "da", 
		"ser", "na", "mi", "kel", "txi", "no", "da", "vid", "ca", "llau", "mi", "lor", "jus", "to", 
		"lei", "rash", "ai", "tor", "ty", "ma", "ra", "ed", "ur", "aia", "la", "ju", "an", "ma",
		"na", "ra", "be", "go", "adri", "ju", "li", "she", "ila", "its", "aso", "ju", "len", "li", "to",
		"ner", "'a", "ull", "'om", "lis", "vic", "ca", "uul"]
	
	beast_attributes = ["", "Poisonous", "Dark", "Acid", "Frozen", "Light", "Red", "Green", "Elder", "Ultra", "Legendary"]
	beast_names = ["Dragon", "Liche", "Giant", "Sandworm", "Troll", "Basilisk", "Gargantua", "Kraken", "Demon"]


func generate_name() -> String:
	var length = randi() % 4 + 2
	var char_name : String = ""
	
	for i in range(length):
		char_name = char_name + _get_chunk()
	
	return char_name.capitalize()


func _get_chunk() -> String:
	return human_chunks[randi() % human_chunks.size()]


func generate_beast_name() -> String:
	return (beast_attributes[randi() % beast_attributes.size()] + " " + beast_names[randi() % beast_names.size()]).strip_edges()
