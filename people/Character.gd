extends Spatial
class_name Character

onready var AnimationPlayerNode : AnimationPlayer = $AnimationPlayer
onready var AnimationPlayerReactionsNode : AnimationPlayer = $AnimationPlayerReactions
onready var BetrayalNode : CPUParticles = $Betrayal

signal character_dead

const WALK_SPEED : float = 0.1

var used_events : Array = []
var action_history : Array = []

var char_name : String = ""
var affinity : int = 50
var betrayal : int = 0
var ethic : int = 0
var moral : int = 0

var dead : bool = false
var selected : bool = false
var pos_origin : Vector3 = Vector3.ZERO
var pos_advance : Vector3 = Vector3(0.0, 1.5, 5.5)


func _ready() -> void:
	char_name = NameGenerator.generate_name()
	ethic = randi() % 3 - 1
	moral = randi() % 3 - 1
	
	$Body.set_frame(randi() % $Body.get_hframes())
	$Body/Head.set_frame(randi() % $Body/Head.get_hframes())
	$Body/Head/Hair.set_frame(randi() % $Body/Head/Hair.get_hframes())
	$Body/Head/Eyes.set_frame(randi() % $Body/Head/Eyes.get_hframes())
	$Body/Head/Mouth.set_frame(randi() % $Body/Head/Mouth.get_hframes())


func _physics_process(delta : float) -> void:
	if dead:
		return
	
	if selected:
		set_translation(lerp(translation, pos_advance, WALK_SPEED))
	else:
		set_translation(lerp(translation, pos_origin, WALK_SPEED))
	
	if betrayal > 70:
		BetrayalNode.set_emitting(true)
	else:
		BetrayalNode.set_emitting(false)


func set_original_position(pos_x : float) -> void:
	pos_origin = Vector3(pos_x, 1.3, 4.2)


func get_alignment() -> String:
	if ethic == 0 and moral == 0:
		return "True Neutral"
	
	var part1 = "Neutral"
	if ethic == -1:
		part1 = "Chaotic"
	elif ethic == 1:
		part1 = "Lawful"
	
	var part2 = "Neutral"
	if moral == -1:
		part2 = "Evil"
	elif moral == 1:
		part2 = "Good"
		
	return part1 + " " + part2


func select() -> void:
	if dead:
		return
	selected = true
	AnimationPlayerNode.play("walk")


func deselect() -> void:
	if dead:
		return
	selected = false
	AnimationPlayerNode.play("walk")


func apply_option(event : Node, option : EventOption) -> String:
	if event.personal and !event.reusable:
		used_events.append(event.id)
	
	if option.action != "":
		action_history.append(option.action)
	
	var affinity_change : int = 0
	
	affinity_change += option.ethic * ethic
	affinity_change += option.moral * moral
	affinity_change += option.nice
	
	affinity += affinity_change
	
	betrayal = clamp(betrayal, 0, 100)
	affinity = clamp(affinity, 0, 100)
	
	if affinity_change > 0:
		AnimationPlayerReactionsNode.play("happy")
	elif affinity_change < 0:
		AnimationPlayerReactionsNode.play("angry")
	
	return AnswerGenerator.create_answer(affinity_change)


func end_of_day() -> void:
	if affinity < 80:
		betrayal += 5
	if affinity < 50:
		betrayal += 8
	if affinity < 20:
		betrayal += 10


func kill() -> void:
	dead = true
	AnimationPlayerNode.play("die")


func _on_text_started() -> void:
	if dead:
		return
	
	if selected:
		AnimationPlayerNode.play("talk")
	else:
		AnimationPlayerNode.play("idle")


func _on_text_completed() -> void:
	if dead:
		return
	
	AnimationPlayerNode.play("idle")


func _on_dead() -> void:
	emit_signal("character_dead")
