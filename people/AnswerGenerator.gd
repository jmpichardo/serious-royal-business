extends Node

var positive : Array = []
var neutral : Array = []
var negative : Array = []
var naming : Array = []


func _ready() -> void:
	positive = ["of course", "nice", "alright", "fantastic", "sounds good", "indeed", "great", "phenomenal", "absolutely"]
	neutral = ["ok", "sure", "well", "...", "as you wish", "let me see", "will do"]
	negative = ["no good", "sorry to hear that", "I'm not sure", "It doesn't sounds good", "I don't like it", "not nice", "sadly", "are you sure?", "not good idea"]
	naming = ["your majesty", "your greatness", "your excellency", "my lord", "your highness"]


func create_answer(affinity_change : int) -> String:
	var answer : String = ""
	var parts = randi() % 2 + 1
	
	for i in range(parts):
		answer = answer + _get_answer_god(affinity_change, i == 0)
	return answer


func _get_answer_god(affinity_change : int, include_naming : bool) -> String:
	var answer : String = ""
	
	if affinity_change > 0:
		answer = answer + positive[randi() % positive.size()]
	elif affinity_change < 0:
		answer = answer + negative[randi() % negative.size()]
	else:
		answer = answer + neutral[randi() % neutral.size()]
	
	if include_naming:
		answer = answer + ", " + naming[randi() % naming.size()]
	
	return answer.capitalize() + ". "
