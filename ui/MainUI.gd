extends Control

signal day_started
signal option_selected
signal event_finished
signal text_started
signal text_completed
signal character_killed

onready var DayNode = $FadeRect/LabelDayValue
onready var AnimationPlayerNode = $AnimationPlayer

onready var GoldNode = $Panel2/GoldValue
onready var ArmyNode = $Panel2/ArmyValue
onready var PopularityNode = $Panel2/PopularityValue

onready var ConversationNode = $Panel/ConversationBox
onready var Button1Node = $OptionsContainer/Button1
onready var Button2Node = $OptionsContainer/Button2
onready var Button3Node = $OptionsContainer/Button3
onready var Button4Node = $OptionsContainer/Button4
onready var ButtonKill = $ButtonKill

var is_event_finished : bool = false


func _ready() -> void:
	DayNode.set_text(str(1))
	clear_options()
	ConversationNode.set_bbcode("")


func _physics_process(delta : float) -> void:
	if ConversationNode.get_bbcode().length() > 0 and ConversationNode.get_percent_visible() < 1.0:
		var speed : float = 1.0 / ConversationNode.get_bbcode().length()
		ConversationNode.set_percent_visible(ConversationNode.get_percent_visible() + speed)
		
		if ConversationNode.get_percent_visible() >= 1.0:
			ConversationNode.set_percent_visible(1.0)
			emit_signal("text_completed")


func update_gold(new_gold : int) -> void:
	GoldNode.set_text(str(new_gold))


func update_army(new_army : int) -> void:
	ArmyNode.set_text(str(new_army))


func update_popularity(new_popularity : int) -> void:
	PopularityNode.set_text(str(new_popularity))


func finish_day(day : int) -> void:
	AnimationPlayerNode.play("day_change")
	clear_options()
	ConversationNode.set_bbcode("")
	DayNode.set_text(str(day))


func day_start() -> void:
	emit_signal("day_started")


func _input(event) -> void:
	if event is InputEventMouseButton:
		if event.is_pressed():
			if is_event_finished:
				is_event_finished = false
				emit_signal("event_finished")


func load_event(event : Event, gold : int, army : int) -> void:
	is_event_finished = false
	clear_options()
	ConversationNode.set_bbcode("")
	
	if event == null:
		is_event_finished = true
		return
		
	ConversationNode.set_bbcode(event.conversation)
	ConversationNode.set_percent_visible(0.0)
	emit_signal("text_started")
	
	var option_index : int = 1
	for option in event.options:
		var disabled : bool = (gold + option.gold_change) < 0 or (army + option.army_change) < 0
		_add_option(option.text, option_index, option.gold_change, option.army_change, disabled)
		option_index += 1
	
	ButtonKill.set_visible(true)


func load_answer(response : String) -> void:
	is_event_finished = true
	clear_options();
	ConversationNode.set_bbcode(response)
	ConversationNode.set_percent_visible(0.0)
	emit_signal("text_started")


func clear_options() -> void:
	Button1Node.set_visible(false)
	Button2Node.set_visible(false)
	Button3Node.set_visible(false)
	Button4Node.set_visible(false)
	ButtonKill.set_visible(false)


func _add_option(text : String, index : int, gold_change : int, army_change : int, disabled : bool) -> void:
	if gold_change < 0:
		text = text + " (" + str(-gold_change) + "G)"
	if army_change < 0:
		text = text + " (" + str(-army_change) + "A)"
	
	if index == 1:
		Button1Node.set_text(text)
		Button1Node.set_visible(true)
		Button1Node.set_disabled(disabled)
	elif index == 2:
		Button2Node.set_text(text)
		Button2Node.set_visible(true)
		Button2Node.set_disabled(disabled)
	if index == 3:
		Button3Node.set_text(text)
		Button3Node.set_visible(true)
		Button3Node.set_disabled(disabled)
	if index == 4:
		Button4Node.set_text(text)
		Button4Node.set_visible(true)
		Button4Node.set_disabled(disabled)


func _button_pressed(index : int) -> void:
	emit_signal("option_selected", index)


func _on_Button1_pressed():
	_button_pressed(0)


func _on_Button2_pressed():
	_button_pressed(1)


func _on_Button3_pressed():
	_button_pressed(2)


func _on_Button4_pressed():
	_button_pressed(3)


func _on_ButtonKill_pressed():
	emit_signal("character_killed")
